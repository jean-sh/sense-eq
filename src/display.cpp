/**
 *   sense-eq
 *   Copyright (C) 2017  Jean Vincent
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <experimental/filesystem>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include "linux/fb.h"
#include "sys/ioctl.h"
#include "sys/mman.h"
#include <sys/stat.h>
#include <unistd.h>

#include <QDebug>

#include "display.h"

namespace fs = std::experimental::filesystem;

const string SENSE_HAT_FB_NAME = "RPi-Sense";
const int SIDE_LENGTH = 8;
const int BUF_SIZE = SIDE_LENGTH * sizeof(uint16_t);

LedDisplay::LedDisplay(const int rotation) {
    m_fb_device = getFbDevicePath();
    m_rotation = rotation;
}

LedDisplay::~LedDisplay() {
	
}

/**
 * Clears the LED matrix with a single color, default is black / off.
 */
void LedDisplay::clear(const Pixel& px) const {
    PixelMatrix8x8 pixels;
    for (array<Pixel, 8>& line : pixels) {
        line.fill(px);
    }
    setPixels(pixels);
}

void LedDisplay::displayEq(const array<uint16_t, 8>& amplitudes) const {
    PixelMatrix8x8 pixels = amplitudesToPixels(amplitudes);
    setPixels(pixels);
}


void LedDisplay::setPixels(PixelMatrix8x8& pixel_matrix) const {
	// TODO wrap in an object for a proper destructor
    int fd = open(m_fb_device.c_str(), O_RDWR);

	fb_var_screeninfo fb_info;
	ioctl(fd, FBIOGET_VSCREENINFO, &fb_info);
	ioctl(fd, FBIOPUT_VSCREENINFO, &fb_info);

	uint16_t* display;
	display = (uint16_t*)mmap(NULL, BUF_SIZE, PROT_READ
								| PROT_WRITE, MAP_SHARED, fd, 0);

    applyRotation(pixel_matrix);
    for (int i = 0 ; i < SIDE_LENGTH ; i++) {
        for (int j = 0 ; j < SIDE_LENGTH ; j++) {
            int index = 8 * i + j;
            display[index] = toRgb565(pixel_matrix[i][j]);
		}
	}

	munmap(display, BUF_SIZE);
    close(fd);
}

PixelMatrix8x8
LedDisplay::amplitudesToPixels(const array<uint16_t, 8>& amplitudes) const {
    PixelMatrix8x8 pixels = PixelMatrix8x8();
    int i = 0;
    for (uint16_t a : amplitudes) {
        // Make sure the amplitudes are capped.
        if (a > 255) {
            a = 255;
        }

        int j = 0;
        while (a >= 32) {
            pixels[i][j] = Colors::rainbow[j] / (2 - (j / 8));
            j++;
            a -= 32;
        }
        if (a > 0) {
            pixels[i][j] = Colors::rainbow[j] / (32 / a);
            j++;
        }
        while (j < 8) {
            pixels[i][j] = Colors::black;
            j++;
        }
        i++;
    }
    return pixels;
}

void LedDisplay::applyRotation(PixelMatrix8x8& pixels) const {
    PixelMatrix8x8 rotatedPixels = PixelMatrix8x8();
    switch (m_rotation) {
    case 90:
        for (int i = 0 ; i < SIDE_LENGTH ; i++) {
            for (int j = 0 ; j < SIDE_LENGTH ; j++) {
                rotatedPixels[i][j] = pixels[SIDE_LENGTH-j-1][i];
            }
        }
        break;
    case 180:
        for (int i = 0 ; i < SIDE_LENGTH ; i++) {
            for (int j = 0 ; j < SIDE_LENGTH ; j++) {
                rotatedPixels[i][j] = pixels[SIDE_LENGTH-i-1][SIDE_LENGTH-j-1];
            }
        }
        break;
    case 270:
        for (int i = 0 ; i < SIDE_LENGTH ; i++) {
            for (int j = 0 ; j < SIDE_LENGTH ; j++) {
                rotatedPixels[i][j] = pixels[j][SIDE_LENGTH-i-1];
            }
        }
        break;
    default:
        return;
    }
    pixels = rotatedPixels;
}

/**
 * Converts an RGB Pixel to the RGB565 format.
 */
uint16_t LedDisplay::toRgb565(const Pixel px) {
    uint16_t r_val = (px.r >> 3) & 0b11111;
    uint16_t g_val = (px.g >> 2) & 0b111111;
    uint16_t b_val = (px.b >> 3) & 0b11111;
    return (r_val << 11) + (g_val << 5) + b_val;
}

string LedDisplay::getFbDevicePath() const
{
	// Replaces part of a string.
	auto strReplace = [](string& str, const string& from, const string& to) {
		size_t start_pos = str.find(from);
		if (start_pos != string::npos) {
			str.replace(start_pos, from.length(), to);
		}	
	};
	
    const string sys_path = "/sys/class/graphics";
    const string sys_path_fb = sys_path + "/fb";
    // Browse directories in /sys/class/graphics.
    for (const auto& p : fs::directory_iterator(sys_path)) {
        const string current_path = p.path().string();
        // Find the ones starting with fb
        if (current_path.compare(0, sys_path_fb.length(), sys_path_fb) == 0) {
            // Get the name of the device.
            std::ifstream name_file;
            name_file.open(current_path + "/name");
            string name;
            name_file >> name;
            name_file.close();

            // If the name matches, return the path of the device in /dev.
            if (name.compare(SENSE_HAT_FB_NAME) == 0) {
                std::cout << "Found device: " << current_path << std::endl;
                string dev_path = current_path;
                strReplace(dev_path, sys_path, "/dev");
                return dev_path;
            }
        }
    }
    // TODO throw an exception if device not found.
    return "";
}




