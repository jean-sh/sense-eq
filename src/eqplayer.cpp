/**
 *   sense-eq
 *   Copyright (C) 2017  Jean Vincent
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <queue>

#include <QDebug>

#include "eqplayer.h"

EqPlayer::EqPlayer(const string& media_path, const int display_rotation) {
    m_processor = std::make_unique<AudioProcessor>();
    m_display = std::make_unique<LedDisplay>(display_rotation);
    m_player = std::make_unique<QMediaPlayer>();
    m_probe = std::make_unique<QAudioProbe>();
    m_playback_thread = std::make_unique<QThread>();

    m_player->setMedia(QUrl::fromLocalFile(media_path.c_str()));
    m_player->setVolume(5);
    m_probe->setSource(m_player.get());
}

void EqPlayer::initializeAudioProcessor(const QString& file_name) const {
    m_processor->createFftwPlan(file_name);
}

void EqPlayer::startPlayback() const {
    QObject::connect(m_probe.get(), SIGNAL(audioBufferProbed(QAudioBuffer)),
                    this, SLOT(processAndDisplay(QAudioBuffer)));
    QObject::connect(m_player.get(),
                     SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),
                     this, SLOT(finishIfEndOfMedia(QMediaPlayer::MediaStatus)));
    m_player->moveToThread(m_playback_thread.get());
    m_playback_thread->start();
    m_player->play();
}

void EqPlayer::processAndDisplay(const QAudioBuffer& buffer) const {
    // This queue adds a delay to the eq and synchronizes with the audio.
    static auto queue = std::queue<QAudioBuffer>();
    if (queue.size() < 6) {
        queue.push(buffer);
    } else {
        QAudioBuffer queued_buffer = queue.front();
        queue.pop();
        vector<double> amplitudes = m_processor->calculateAmplitudes(queued_buffer);
        array<uint16_t, 8> values_for_display = prepareForDisplay(amplitudes);
        m_display->displayEq(values_for_display);
    }
}

void EqPlayer::printInfos(const QAudioBuffer& buffer) const {
    m_processor->printInfos(buffer);
}

array<uint16_t, 8>
EqPlayer::prepareForDisplay(const vector<double>& amplitudes) const {
    auto averageOverRange = [](const vector<double>& vec, uint begin, uint end) {
        double sum = 0;
        for (uint i = begin ; i < end ; i++) {
            sum += vec[i];
        }
        return sum / (end - begin);
    };

    // The boundary multipliers are defined as a fraction of 481, which is
    // the length of the result array of an FFT with an input of 960 samples.
    // Their product with the actual length of the array gives the boundaries
    // that will be used to delimits the eight bands of the eq (by averaging
    // values between two boundaries).
    const array<double, 9> boundary_multipliers = {
                                    0., 8./481, 24./481, 48./481, 80./481,
                                    112./481, 160./481, 224./481, 300./481};
    array<uint, 9> boundaries;
    for (uint i = 0 ; i < boundary_multipliers.size() ; i++) {
        boundaries[i] = boundary_multipliers[i] * m_processor->outSize();
    }

    // These correction factors are used to lower the low end
    // and raise the high end for a more visually balanced eq.
    const array<double, 8> correction_factors = {0.85, 0.9, 0.95, 1.0,
                                                    1.0, 1.0, 1.05, 1.1};
    array<double, 8> amplitudes_double = array<double, 8>();
    array<uint16_t, 8> amplitudes_uint16 = array<uint16_t, 8>();

    for (uint i = 0 ; i < amplitudes_double.size() ; i++) {
        // Reduce the vector of amplitudes to an array of 8.
        amplitudes_double[i] = averageOverRange(amplitudes,
                                                  boundaries[i],
                                                  boundaries[i+1]);
        // Square the amplitudes for more dynamic visual variations.
        amplitudes_double[i] *= amplitudes_double[i];

        amplitudes_double[i] *= correction_factors[i];
        // Rescale to values between 0 and 255.
        amplitudes_double[i] = (amplitudes_double[i] / 32) * 255;
        // Cast to uint.
        amplitudes_uint16[i] = amplitudes_double[i];
    }
    return amplitudes_uint16;
}

void EqPlayer::finishIfEndOfMedia(const QMediaPlayer::MediaStatus& status) {
    if (status == QMediaPlayer::EndOfMedia) {
        m_playback_thread->quit();
        m_display->clear();
        emit finished();
    }
}
