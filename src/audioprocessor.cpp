/**
 *   sense-eq
 *   Copyright (C) 2017  Jean Vincent
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>

#include <QAudioDecoder>
#include <QDebug>

#include "audioprocessor.h"

AudioProcessor::AudioProcessor() {
    m_in = nullptr;
    m_out = nullptr;
}

AudioProcessor::~AudioProcessor() {
    fftw_destroy_plan(m_plan);
    fftw_free(m_in);
    fftw_free(m_out);
}

void AudioProcessor::createFftwPlan(const QString& file_name) {
    QAudioDecoder decoder;
    QAudioBuffer buffer;
    decoder.setSourceFilename(file_name);
    decoder.start();
    // Take a buffer later in the file to avoid inconsistencies
    // at the beginning of files.
    while (decoder.position() < 300) {
        if (decoder.bufferAvailable()) {
            buffer = decoder.read();
        }
    }
    printInfos(buffer);

    // The size of the out array created by FFTW is the
    // size of the in array divided by 2, plus 1.
    m_in_size = buffer.frameCount();
    m_out_size = (m_in_size / 2) + 1;

    // Create the FFTW plan that will be used to calculate the FFTs.
    fftw_init_threads();
    fftw_plan_with_nthreads(3);
    m_in = fftw_alloc_real(sizeof(double) * m_in_size);
    m_out = fftw_alloc_complex(sizeof(fftw_complex) * m_out_size);
    m_plan = fftw_plan_dft_r2c_1d(m_in_size, m_in, m_out, FFTW_MEASURE);
}

void AudioProcessor::printInfos(const QAudioBuffer& buffer) const {
    QAudioFormat format = buffer.format();
    qDebug() << "Codec:\t\t" << format.codec();
    qDebug() << "Byte order:\t" << format.byteOrder();
    qDebug() << "Sample rate:\t" << format.sampleRate();
    qDebug() << "Sample size:\t" << format.sampleSize();
    qDebug() << "Sample type:\t" << format.sampleType();
    qDebug() << "Frame count:\t" << buffer.frameCount();
    qDebug() << "Sample count:\t" << buffer.sampleCount();
}

vector<double>
AudioProcessor::calculateAmplitudes(const QAudioBuffer& buffer) const {
    const QAudioBuffer::S16S* frames = buffer.constData<QAudioBuffer::S16S>();
    for (size_t i = 0 ; i < m_in_size ; i++) {
        // Average between channels.
        m_in[i] = static_cast<double>(frames[i].average());

        // Apply windowing function (Hann).
        const double multiplier = 0.5 * (1 - cos(2*M_PI*i/m_in_size-1));
        m_in[i] *= multiplier;
    }

    // Calculate the FFT.
    fftw_execute(m_plan);

    // Calculate the amplitudes in a dB scale.
    vector<double> amplitudes = vector<double>();

    for (size_t i = 0 ; i < m_out_size ; i++) {
        // The formula for the amplitude from an FFT result is:
        // √(x² + y²) where x is the real part and y the imaginary part.
        // We then get the base 10 logarithm for a dB scale.
        const double a = sqrt(m_out[i][0] * m_out[i][0] + m_out[i][1] * m_out[i][1]);
        amplitudes.push_back(log10(a));
    }
    return amplitudes;
}
