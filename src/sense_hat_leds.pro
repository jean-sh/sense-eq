QT += core multimedia
QT -= gui

CONFIG += c++14 console
CONFIG -= app_bundle

TARGET = sense-eq
TEMPLATE = app

unix:LIBS += -lstdc++fs -lfftw3_threads -lfftw3 -lm

SOURCES += \
    sense-eq.cpp \
    display.cpp \
    audioprocessor.cpp \
    eqcontroller.cpp

HEADERS += \
    display.h \
    audioprocessor.h \
    eqcontroller.h
