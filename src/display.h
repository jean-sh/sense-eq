/**
 *   sense-eq
 *   Copyright (C) 2017  Jean Vincent
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DISPLAY_H
#define DISPLAY_H

#include <array>
#include <stdint.h>
#include <string>

struct Pixel {
	uint8_t r;
	uint8_t g;
    uint8_t b;

    Pixel operator/(const double d) const {
        return {
            static_cast<uint8_t>(r / d),
            static_cast<uint8_t>(g / d),
            static_cast<uint8_t>(b / d)
        };
    }
};

using std::string;
using std::array;
using PixelMatrix8x8 = array<array<Pixel, 8>, 8>;

namespace Colors {
    const Pixel black = {0, 0, 0};
    const Pixel white = {255, 255, 255};

    const Pixel magenta = {192, 0, 192};
    const Pixel violet = {96, 0, 192};
    const Pixel blue = {48, 48, 192};
    const Pixel electric = {0, 96, 192};
    const Pixel cyan = {0, 192, 192};
    const Pixel emerald = {0, 192, 96};
    const Pixel green = {0, 192, 0};
    const Pixel citrus = {96, 192, 0};
    const Pixel yellow = {192, 192, 0};
    const Pixel orange = {192, 96, 0};
    const Pixel red = {192, 0, 0};
    const Pixel fashion = {192, 0, 96};

    const array<Pixel, 8> rainbow = {blue, electric, emerald, citrus,
                                     fashion, magenta, orange, red};
}

class LedDisplay {
public:
    LedDisplay(const int rotation = 0);
    ~LedDisplay();
    void clear(const Pixel& px = {0, 0, 0}) const;
    void displayEq(const array<uint16_t, 8>& amplitudes) const;
    void setPixels(PixelMatrix8x8& pixel_matrix) const;

private:
    static uint16_t toRgb565(const Pixel px);
    
    string getFbDevicePath() const;
    PixelMatrix8x8 amplitudesToPixels(const array<uint16_t, 8>& amplitudes) const;
    void applyRotation(PixelMatrix8x8& pixels) const;

    string m_fb_device;
    int m_rotation;
};

#endif // DISPLAY_H
