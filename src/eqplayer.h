/**
 *   sense-eq
 *   Copyright (C) 2017  Jean Vincent
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EQPLAYER_H
#define EQPLAYER_H

#include <memory>

#include <QAudioProbe>
#include <QMediaPlayer>
#include <QObject>
#include <QThread>

#include "audioprocessor.h"
#include "display.h"

using std::array;
using std::string;
using std::vector;

class EqPlayer : public QObject {
    Q_OBJECT
public:
    explicit EqPlayer(const string& media_path, const int display_rotation = 0);
    void initializeAudioProcessor(const QString& file_name) const;
    void startPlayback() const;

signals:
    void finished();

public slots:
    void processAndDisplay(const QAudioBuffer& buffer) const;
    void printInfos(const QAudioBuffer& buffer) const;

private:
    array<uint16_t, 8> prepareForDisplay(const vector<double>& amplitudes) const;

    std::unique_ptr<AudioProcessor> m_processor;
    std::unique_ptr<LedDisplay> m_display;
    std::unique_ptr<QMediaPlayer> m_player;
    std::unique_ptr<QAudioProbe> m_probe;
    std::unique_ptr<QThread> m_playback_thread;

private slots:
    void finishIfEndOfMedia(const QMediaPlayer::MediaStatus& status);
};

#endif // EQPLAYER_H
