/**
 *   sense-eq
 *   Copyright (C) 2017  Jean Vincent
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AUDIOPROCESSOR_H
#define AUDIOPROCESSOR_H

#include <fftw3.h>

#include <QAudioBuffer>
#include <QObject>

using std::vector;

class AudioProcessor : public QObject {
    Q_OBJECT
public:
    explicit AudioProcessor();
    ~AudioProcessor();

    void createFftwPlan(const QString& file_name);
    void printInfos(const QAudioBuffer& buffer) const;
    vector<double> calculateAmplitudes(const QAudioBuffer& buffer) const;
    size_t inSize() const {return m_in_size;}
    size_t outSize() const {return m_out_size;}

private:
    fftw_plan m_plan;
    double* m_in;
    fftw_complex* m_out;
    size_t m_in_size;
    size_t m_out_size;
};

#endif // AUDIOPROCESSOR_H
