/**
 *   sense-eq
 *   Copyright (C) 2017  Jean Vincent
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <memory>

#include <QCoreApplication>
#include <QObject>

#include "eqplayer.h"

using std::string;
using std::vector;

/**
 * @brief Simple input parser by iain
 * source:
 * https://stackoverflow.com/questions/865668/how-to-parse-command-line-arguments-in-c
 */
class InputParser {
    public:
        InputParser(int& argc, char **argv) {
            for (int i = 1 ; i < argc ; i++) {
                this->tokens.push_back(string(argv[i]));
            }
        }
        const string& getCmdOption(const string& option) const {
            vector<string>::const_iterator itr;
            itr = std::find(this->tokens.begin(), this->tokens.end(), option);
            if (itr != this->tokens.end() && ++itr != this->tokens.end()) {
                return *itr;
            }
            static const string empty_string("");
            return empty_string;
        }
        bool cmdOptionExists(const string& option) const {
            return std::find(this->tokens.begin(), this->tokens.end(), option)
                   != this->tokens.end();
        }
    private:
        vector<string> tokens;
};

int main(int argc, char* argv[]) {
    QCoreApplication a(argc, argv);

    InputParser parser(argc, argv);
    if (!parser.cmdOptionExists("-i") || parser.cmdOptionExists("-h")) {
        std::cout << "Usage:\n\tsense-eq -i path/to/input [OPTIONS]\n"
                     "Options:\n\t-r\trotation of the display:"
                            "0 (default), 90, 180 or 270\n"
                     "\t-v\tvolume: 0-100 (default: 10)\n"
                     "\t-h\tprint this help" << std::endl;
        return 0;
    }
    string path = parser.getCmdOption("-i");

    int rotation = 0;
    if (parser.cmdOptionExists("-r")) {
        rotation = std::stoi(parser.getCmdOption("-r"));
        if (rotation != 0 && rotation != 90 && rotation != 180 && rotation != 270) {
            std::cout << "Please enter 0, 90, 180 or 270 as an angle." << std::endl;
            return 0;
        }
    }

    int volume = 10;
    if (parser.cmdOptionExists("-v")) {
        volume = std::stoi(parser.getCmdOption("-v"));
        if (volume < 0 || volume > 100) {
            std::cout << "The volume must be between 0 and 100" << std::endl;
            return 0;
        }
    }

    EqPlayer eq_player(path, rotation);
    eq_player.initializeAudioProcessor(path.c_str());
    eq_player.startPlayback();
    QObject::connect(&eq_player, SIGNAL(finished()), &a, SLOT(quit()));
    return a.exec();
}
